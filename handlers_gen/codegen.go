package main

import (
	"encoding/json"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	//"net/http"
	"text/template"
	"os"
	"strings"
)

// код писать тут

// return &ResourceError{URL: url, Err: err}

//
//func authMiddleware(next http.Handler) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		value := r.Header.Get("X-Auth")
//		if value == "" {
//			http.Error(w, "Internal server error", http.StatusForbidden)
//			return
//		}
//		next.ServeHTTP(w, r)
//	})
//}

//func (h *SomeStructName ) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	switch r.URL.Path {
//	case "...":
//		h.wrapperDoSomeJob(w, r)
//	default:
//		// 404
//	}
//}

type Case struct {
	Path string
	MethodName string
}

type serveHTTPTpl struct {
	StructName string
	Cases []Case
}

var (
	serveHTTPFuncsTpl = template.Must(template.New("serveHTTPFuncsTpl").Parse(`
{{range $structName, $cases := .}}
func (srv *{{$structName}}) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	{{range $cases}}
	case "{{.Path}}":
		srv.handler{{.MethodName}}(w, r)
	{{end}}
	default:
		http.Error(w, "not found", 404)
	}
}
{{end}}
`))
//func (h *SomeStructName ) wrapperDoSomeJob() {
//	// заполнение структуры params
//	// валидирование параметров
//	res, err := h.DoSomeJob(ctx, params)
//	// прочие обработки
//}

	handlerMethodTpl = template.Must(template.New("handlerMethodTpl").Parse(`
{{range $structName, $cases := .}}
	{{range $cases}}
func (srv *{{$structName}}) handler{{.MethodName}}(w http.ResponseWriter, r *http.Request) {
	// заполнение структуры params
	// валидирование параметров	
	res, err := srv.{{.MethodName}}(ctx, params)
	// прочие обработки
}
	{{end}}
{{end}}
`))
)

type FuncCodegenParams struct {
	URL    string
	Auth   bool
	Method string
}

func main() {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, os.Args[1], nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	//out, _ := os.Create(os.Args[2])
	//
	//fmt.Fprintln(out, `package `+node.Name.Name)
	//fmt.Fprintln(out) // empty line
	//fmt.Fprintln(out, `import "encoding/binary"`)
	//fmt.Fprintln(out, `import "bytes"`)
	//fmt.Fprintln(out) // empty line
	h := map[string][]Case{
		"MyApi": []Case{
			{
				Path:       "/user/profile",
				MethodName: "Profile",
			},
			{
				Path:       "/user/create",
				MethodName: "Create",
			},
		},
		"OtherApi": []Case{
			{
				Path:       "/user/create",
				MethodName: "Create",
			},
		},
	}
	serveHTTPFuncsTpl.Execute(os.Stdout, h)
	handlerMethodTpl.Execute(os.Stdout, h)

	for _, f := range node.Decls {
		g, ok := f.(*ast.FuncDecl)
		if !ok {
			fmt.Printf("SKIP %T is not *ast.FuncDecl\n", f)
			continue
		}
		if g.Doc == nil {
			fmt.Printf("SKIP func %#v doesnt have comments\n", g.Name.Name)
			continue
		}
		r := strings.Replace(g.Doc.List[0].Text, "// apigen:api ", "", 1)
		funcCodegenParams := &FuncCodegenParams{}
		err := json.Unmarshal([]byte(r), funcCodegenParams)
		if err != nil {
			fmt.Printf("SKIP func %#v doesnt have apigen mark\n", g.Name.Name)
			continue
		}
		fmt.Printf("process func %s\n", g.Name.Name)
		for _, v := range g.Recv.List {
			switch xv := v.Type.(type) {
			case *ast.StarExpr:
				if si, ok := xv.X.(*ast.Ident); ok {
					fmt.Println(si.Name)
				}
			case *ast.Ident:
				fmt.Println(xv.Name)
			}
		}
		//fmt.Println(g.Recv.List)
		//fmt.Println(g.Type.Params)

	}
}
